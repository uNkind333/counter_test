import Counter from './counter'
let elem = null;
let counterCmp;
let defaultConfig = {
    name: 'Cool counter',
    inc: 1,
    initValue: 1
}
const createElem = () => {
    elem = document.createElement('div');
    elem.id = 'root';
    document.body.appendChild(elem)
}

const destroyElem = () => {
    document.body.removeChild(elem);
    elem = null;
}

const createComponent = () => {
    counterCmp = new Counter(elem, defaultConfig)
}

const destroyComponent = () => {
    counterCmp = null;
}

describe('Counter', () => {
    beforeEach(createElem)
    afterEach(destroyElem)

    it('should be created ', () => {
        let counter = new Counter(
            elem,
            defaultConfig
        )

        expect(counter).toBeDefined();
        expect(counter.name).toMatch(defaultConfig.name);
        expect(counter.inc).toEqual(defaultConfig.inc);
        expect(counter.value).toEqual(defaultConfig.initValue);

    })

    it('should be created with default options', () => {
        let counter = new Counter(elem);
        expect(counter.name).toMatch('');
        expect(counter.inc).toEqual(1);
        expect(counter.value).toEqual(0);
        expect(counter.maxVal).toEqual(10);
    })

    it('should throw without HTMLElement', () => {
        expect(function() {
            new Counter()
        }).toThrow();
    })

})

describe('test initial methods', function() {
    beforeEach(function() {
        createElem();
        createComponent();
    })

    afterEach(function() {
        destroyElem();
        destroyComponent();
    })
    describe('test Counter render', () => {


        it('it should create root element', () => {
            expect(counterCmp.cmpRoot instanceof HTMLElement).toBe(true)
        })

    })

    describe('test iniEvents', () => {
        beforeEach(function() {
            destroyComponent()
        })
        it('should call InitEvents', () => {

            let eventsSpy = spyOn(Counter.prototype, 'initEvents');
            createComponent();

            expect(eventsSpy).toHaveBeenCalled();
        })

        it('should increment', () => {
            let incrementSpy = spyOn(Counter.prototype, 'onIncrement').and.callThrough()
            createComponent();
            let val = counterCmp.value;
            let e = new MouseEvent('click');
            counterCmp.cmpRoot.getElementsByClassName('js-increment')[0].dispatchEvent(e)
            expect(incrementSpy).toHaveBeenCalled();
            expect(counterCmp.value).toEqual(val + defaultConfig.inc)
            expect(counterCmp.cmpRoot.getElementsByClassName('js-increment')[0] instanceof HTMLElement).toBe(true);
        })
        //should decrement
        it('should decrement', () => {
            let incrementSpy = spyOn(Counter.prototype, 'onDecrement').and.callThrough()
            createComponent();
            let val = counterCmp.value;
            let e = new MouseEvent('click');
            counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0].dispatchEvent(e)
            expect(incrementSpy).toHaveBeenCalled();
            expect(counterCmp.value).toEqual(val - defaultConfig.inc)
            expect(counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0] instanceof HTMLElement).toBe(true);
        })
        //check positive value
        it('should check positive value ', () => {
            let incrementSpy = spyOn(Counter.prototype, 'onDecrement').and.callThrough()
            createComponent();
            let val = counterCmp.value;
            let e = new MouseEvent('click');
            for (let i = 0; i < 3; i++) {
                counterCmp.cmpRoot.getElementsByClassName('js-decrement')[0].dispatchEvent(e);
            }
            expect(counterCmp.value).toEqual(0);
        })
    })


    describe('set max', () => {
        it('should set max value on click', () => {
            destroyComponent();
            let setMaxSpy = spyOn(Counter.prototype, 'onSetMax').and.callThrough()
            createComponent();
            let e = new MouseEvent('click');
            counterCmp.cmpRoot.getElementsByClassName('js-set-max')[0].dispatchEvent(e)
            expect(setMaxSpy).toHaveBeenCalled()
            expect(counterCmp.value).toEqual(counterCmp.maxVal)
            expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(counterCmp.maxVal)
        })
        // set correct max value
        it('should set correct max value', () => {
            destroyComponent();
            const setMaxSpy = spyOn(Counter.prototype, 'onSetMax').and.callThrough();
            createComponent();
            const e = new MouseEvent('click');
            counterCmp.cmpRoot.getElementsByClassName('js-set-max')[0].dispatchEvent(e);
            expect(counterCmp.maxVal).toEqual(10);
        })
    })

    describe('set min', () => {
        it('should set min value on click', () => {
            destroyComponent();
            let setMinSpy = spyOn(Counter.prototype, 'onSetMin').and.callThrough()
            createComponent()
            let counterCmp = new Counter(elem, {
                name: "ASD",
                inc: 1,
                initValue: 5
            })
            let e = new MouseEvent('click');
            counterCmp.cmpRoot.getElementsByClassName('js-set-min')[0].dispatchEvent(e)
            expect(setMinSpy).toHaveBeenCalled()
            expect(counterCmp.value).toEqual(defaultConfig.initValue)
            expect(+counterCmp.cmpRoot.getElementsByClassName('js-val')[0].innerText).toEqual(defaultConfig.initValue)
        })
    })

})